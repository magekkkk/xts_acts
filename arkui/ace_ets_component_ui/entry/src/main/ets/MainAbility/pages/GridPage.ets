/**
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import {MessageManager,Callback} from '../utils/MessageManager';

@Entry
@Component
struct GridPage {
  @State Number: String[] = ['0', '1', '2', '3', '4']
  @State columnsTemplate: string = '1fr 1fr 1fr 1fr 1fr'
  @State rowsTemplate: string = '1fr 1fr 1fr 1fr 1fr'
  @State columnsGap: number = 10
  @State rowsGap: number = 10
  @State scrollBar: BarState = BarState.Off
  @State scrollBarColor: Color = Color.Grey
  @State scrollBarWidth: number = 20
  @State cachedCount: number = 1
  @State editMode: boolean = false
  @State layoutDirection: GridDirection = GridDirection.Column
  @State maxCount: number = Infinity
  @State minCount: number = 1
  @State cellLength: number = 40
  @State multiSelectable: boolean = false
  @State supportAnimation: boolean = false
  @State onScrollIndex: number = 2
  @State buttonName: string = 'next page'
  @State onItemDragStart: string = 'onItemDragStart'
  @State onItemDragEnter: string = 'onItemDragEnter'
  @State onItemDragMove: string = 'onItemDragMove'
  @State onItemDragLeave: string = 'onItemDragLeave'
  @State onItemDrop: string = 'onItemDrop'
  @State myKey: number = 0
  scroller: Scroller = new Scroller()
  messageManager:MessageManager = new MessageManager()

  onPageShow() {
    console.info('GridPage onPageShow')
    globalThis.value = {
      name:'messageManager',message:this.messageManager
    }
    let callback:Callback = (message:any) => {
      console.error('message = ' + message.name + "--" + message.value)
      if (message.name == 'columnsTemplate') {
        this.columnsTemplate = message.value
      }
      if (message.name == 'rowsTemplate') {
        this.rowsTemplate = message.value
      }
      if (message.name == 'columnsGap') {
        this.columnsGap = message.value
      }
      if (message.name == 'rowsGap') {
        this.rowsGap = message.value
      }
      if (message.name == 'scrollBar') {
        this.scrollBar = message.value
      }
      if (message.name == 'scrollBarColor') {
        this.scrollBarColor = message.value
      }
      if (message.name == 'scrollBarWidth') {
        this.scrollBarWidth = message.value
      }
      if (message.name == 'cachedCount') {
        this.cachedCount = message.value
      }
      if (message.name == 'editMode') {
        this.editMode = message.value
      }
      if (message.name == 'layoutDirection') {
        this.layoutDirection = message.value
      }
      if (message.name == 'maxCount') {
        this.maxCount = message.value
      }
      if (message.name == 'minCount') {
        this.minCount = message.value
      }
      if (message.name == 'cellLength') {
        this.cellLength = message.value
      }
      if (message.name == 'multiSelectable') {
        this.multiSelectable = message.value
      }
      if (message.name == 'supportAnimation') {
        this.supportAnimation = message.value
      }
    }
    this.messageManager.registerCallback(callback)
  }

  build() {
    Column({ space: 5 }) {
      Text('default').fontColor(0xCCCCCC).fontSize(9).width('90%')
      Grid() {
        GridItem() {
          Text("abc").fontSize(16).backgroundColor(0xF9CF93).width('100%')
            .height(40).textAlign(TextAlign.Center)
        }.width('90%')
      }.columnsTemplate('1fr')
      .rowsTemplate('1fr')
      .height(50)
      .key('grid_default')

      Grid() {
        ForEach(this.Number, (day: string) => {
          ForEach(this.Number, (day: string) => {
            GridItem() {
              Text(day)
                .fontSize(16)
                .backgroundColor(0xF9CF93)
                .width(40)
                .height(40)
                .textAlign(TextAlign.Center)
                .key(day)
            }
          }, day => day)
        }, day => day)
      }
      .columnsTemplate(this.columnsTemplate)
      .rowsTemplate(this.rowsTemplate)
      .columnsGap(this.columnsGap)
      .rowsGap(this.rowsGap)
      .scrollBar(this.scrollBar)
      .cachedCount(this.cachedCount)
      .editMode(this.editMode)
      .layoutDirection(this.layoutDirection)
      .maxCount(this.maxCount)
      .minCount(this.minCount)
      .cellLength(this.cellLength)
      .multiSelectable(this.multiSelectable)
      .supportAnimation(this.supportAnimation)
      .width('90%')
      .key('grid1')
      .backgroundColor(0xFAEEE0)
      .height(200)
      .onItemDragStart((event: ItemDragInfo, itemIndex: number) => {
        this.onItemDragStart = 'onItemDragStart:succ'
        console.info('DragInfo_x: ' + event.x + ' DragInfo_y: ' + event.y)
        console.info(itemIndex.toString())
      })
      .onItemDragEnter((event: ItemDragInfo) => {
        this.onItemDragEnter = 'onItemDragEnter:succ'
        console.info('DragInfo_x: ' + event.x + ' DragInfo_y: ' + event.y)
      })
      .onItemDragMove((event: ItemDragInfo, itemIndex: number, insertIndex: number) => {
        this.onItemDragMove = 'onItemDragMove:succ'
        console.info('DragInfo_x: ' + event.x + ' DragInfo_y: ' + event.y + ' itemIndex: ' + itemIndex + ' insertIndex: ' + insertIndex)
      })
      .onItemDragLeave((event: ItemDragInfo, itemIndex: number) => {
        this.onItemDragLeave = 'onItemDragLeave:succ'
        console.info('DragInfo_x: ' + event.x + ' DragInfo_y: ' + event.y + ' itemIndex: ' + itemIndex)
      })
      .onItemDrop((event: ItemDragInfo, itemIndex: number, insertIndex: number, isSuccess: boolean) => {
        this.onItemDrop = 'onItemDrop:succ'
        console.info('DragInfo_x: ' + event.x + ' DragInfo_y: ' + event.y + ' itemIndex: ' + itemIndex + ' insertIndex: ' + insertIndex + ' isSuccess: ' + isSuccess)
      })

      Text('scroll').fontColor(0xCCCCCC).fontSize(9).width('90%')
      Grid(this.scroller) {
        ForEach(this.Number, (day: string) => {
          ForEach(this.Number, (day: string) => {
            GridItem() {
              Text(day)
                .fontSize(16)
                .backgroundColor(0xF9CF93)
                .width('100%')
                .height(80)
                .textAlign(TextAlign.Center)
            }
          }, day => day)
        }, day => day)
      }
      .columnsTemplate('1fr 1fr 1fr 1fr 1fr')
      .columnsGap(10)
      .rowsGap(10)
      .key('grid2')
      .scrollBar(this.scrollBar)
      .scrollBarColor(this.scrollBarColor)
      .scrollBarWidth(this.scrollBarWidth)
      .onScrollIndex((first: number) => {
        this.onScrollIndex = first
        console.info(first.toString())
      })
      .width('90%')
      .backgroundColor(0xFAEEE0)
      .height(250)

      Button(this.buttonName)
        .key('button')
        .onClick(() => { // 点击后滑到下一页
          this.buttonName = 'clicked'
          this.scroller.scrollPage({ next: true })
        })
      Text('onScrollIndex:' + this.onScrollIndex).key('onScrollIndex')
      Text(this.onItemDragStart).key('onItemDragStart')
      Text(this.onItemDragEnter).key('onItemDragEnter')
      Text(this.onItemDragMove).key('onItemDragMove')
      Text(this.onItemDragLeave).key('onItemDragLeave')
      Text(this.onItemDrop).key('onItemDrop')
    }.width('100%').margin({ top: 5 })
  }
}