/*
 * Copyright (c) 2023-2030 iSoftStone Information Technology (Group) Co.,Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from "@ohos/hypium";
import router from '@system.router';
import CommonFunc from "./../../../../MainAbility/common/Common"
import {MessageManager,Callback} from './../../../../MainAbility/common/MessageManager';
export default function AlignStart_AddOffset() {
  describe('AlignStart_AddOffset', function () {
    beforeEach(async function (done) {
      let options = {
        uri: 'MainAbility/pages/Flex/justifyContent/Start/AlignStart_AddOffset'
      }
      try {
        router.clear();
        let pages = router.getState();
        console.info("get AlignStart_AddOffset state success " + JSON.stringify(pages));
        if (!("AlignStart_AddOffset" == pages.name)) {
          console.info("get AlignStart_AddOffset state pages.name " + JSON.stringify(pages.name));
          let result = await router.push(options);
          await CommonFunc.sleep(2000);
          console.info("push AlignStart_AddOffset page success " + JSON.stringify(result));
        }
      } catch (err) {
        console.error("push AlignStart_AddOffset page error " + JSON.stringify(err));
        expect().assertFail();
      }
      done()
    });

    afterEach(async function () {
      await CommonFunc.sleep(2000);
      console.info("AlignStart_AddOffset after each called");
    });

    /**
     * @tc.number    SUB_ACE_FLEX_JUSTIFYCONTENT_FLEXALIGN_START_TEST_0900
     * @tc.name      Align_Start_Row_AddOffset
     * @tc.desc      The interface display of the component that sets the offset position when drawing
     */
    it('SUB_ACE_FLEX_JUSTIFYCONTENT_FLEXALIGN_START_TEST_0900', 0, async function (done) {
      console.info('[SUB_ACE_FLEX_JUSTIFYCONTENT_FLEXALIGN_START_TEST_0900] START');
      globalThis.value.message.notify({name:'OneOffset', value:{ x: 10, y: 15 }});
      await CommonFunc.sleep(3000);
      let strJson = getInspectorByKey('Start_AddOffset_01');
      let obj = JSON.parse(strJson);
      expect(obj.$type).assertEqual('Flex');
      expect(obj.$attrs.constructor.direction).assertEqual('FlexDirection.Row');
      expect(obj.$attrs.constructor.justifyContent).assertEqual('FlexAlign.Start');
      let Start_AddOffset_011 = CommonFunc.getComponentRect('Start_AddOffset_011');
      let Start_AddOffset_012 = CommonFunc.getComponentRect('Start_AddOffset_012');
      let Start_AddOffset_013 = CommonFunc.getComponentRect('Start_AddOffset_013');
      let Start_AddOffset_01 = CommonFunc.getComponentRect('Start_AddOffset_01');
      expect(Start_AddOffset_011.top - Start_AddOffset_01.top).assertEqual(vp2px(15));
      expect(Start_AddOffset_011.left - Start_AddOffset_01.left).assertEqual(vp2px(10));
      expect(Start_AddOffset_012.top).assertEqual(Start_AddOffset_013.top);
      expect(Start_AddOffset_013.left).assertEqual(Start_AddOffset_012.right);
      expect(Start_AddOffset_01.right - Start_AddOffset_013.right).assertEqual(vp2px(50));
      expect(Start_AddOffset_011.right - Start_AddOffset_011.left).assertEqual(vp2px(150));
      expect(Start_AddOffset_012.right - Start_AddOffset_012.left).assertEqual(vp2px(150));
      expect(Start_AddOffset_013.right - Start_AddOffset_013.left).assertEqual(vp2px(150));
      expect(Start_AddOffset_011.bottom - Start_AddOffset_011.top).assertEqual(vp2px(50));
      expect(Start_AddOffset_012.bottom - Start_AddOffset_012.top).assertEqual(vp2px(100));
      expect(Start_AddOffset_013.bottom - Start_AddOffset_013.top).assertEqual(vp2px(150));
      console.info('[SUB_ACE_FLEX_JUSTIFYCONTENT_FLEXALIGN_START_TEST_0900] END');
      done();
    });
  })
}
